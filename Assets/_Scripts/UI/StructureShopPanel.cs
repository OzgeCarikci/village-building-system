﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using StructureDatas;

namespace VillageBuilding
{
    public class StructureShopPanel : UIPanel
    {
        public GameObject StructureUIPrefab;

        public Transform CommonUIParent;
        public Transform UniqueUIParent;

        public StructureData StructureData;

        private List<UIStructure> mCommonStructures = new List<UIStructure>();
        private List<UIStructure> mUniqueStructures = new List<UIStructure>();
        private float mCardWidth;

        public override void Initialize(UIManager uiManager)
        {
            base.Initialize(uiManager);

            ResourceStructures();
            InitStructureCards();
        }

        public void ResourceStructures()
        {
            var text = Resources.Load("Structures") as TextAsset;
            StructureData = JsonConvert.DeserializeObject<StructureData>(text.ToString());
        }

        private void InitStructureCards()
        {
            mCommonStructures.Clear();
            mUniqueStructures.Clear();

            foreach (var item in StructureData.Structures)
            {
                var structureObj = Instantiate(StructureUIPrefab);
                var sprite = UIManager.GraphicManager.GetStructureSprite(item.ImageName);

                switch ((StructureTypes)item.Type)
                {
                    case StructureTypes.Common:
                        structureObj.transform.SetParent(CommonUIParent);
                        mCommonStructures.Add(structureObj.GetComponent<UIStructure>());
                        break;

                    case StructureTypes.Unique:
                        structureObj.transform.SetParent(UniqueUIParent);
                        mUniqueStructures.Add(structureObj.GetComponent<UIStructure>());
                        break;
                }

                var uiStructure = structureObj.GetComponent<UIStructure>();

                uiStructure.Initialize(UIManager, item, sprite);
                structureObj.GetComponent<InGameButton>().Initialize(UIManager, OnClickUIStructureButton, item);

                var cardRect = structureObj.GetComponent<RectTransform>();
                cardRect.localScale = Vector3.one;
                mCardWidth = cardRect.GetComponent<LayoutElement>().minWidth;
            }

            SetScrollList();
        }

        private void SetScrollList()
        {
            var commonRect = CommonUIParent.GetComponent<RectTransform>();
            var uniqueRect = UniqueUIParent.GetComponent<RectTransform>();

            commonRect.sizeDelta = new Vector2((mCommonStructures.Count * mCardWidth) + ((mCommonStructures.Count + 1) * 50f), commonRect.sizeDelta.y);
            uniqueRect.sizeDelta = new Vector2((mUniqueStructures.Count * mCardWidth) + ((mUniqueStructures.Count + 1) * 50f), uniqueRect.sizeDelta.y);
        }

        public void OnClickUIStructureButton(StructureItem structureItem)
        {
            UIManager.GameManager.InstantiateStructure(structureItem);
            HidePanel();
        }

        private void Refresh()
        {
            foreach (var uiStructure in mUniqueStructures)
            {
                var isGenerated = UIManager.GameManager.GeneratedUniqueIds.Contains(uiStructure.mStructureItem.Id);
                uiStructure.Refresh(!isGenerated);
            }
        }

        public override void ShowPanel()
        {
            base.ShowPanel();
            Refresh();
        }

        public override void HidePanel()
        {
            base.HidePanel();
        }
    }
}
