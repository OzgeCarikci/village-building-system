﻿
using UnityEngine;

namespace VillageBuilding
{
    public class UIManager : VillageBuildingBehaviour
    {
        public UIPanel[] Panels;

        private GameManager mGameManager;
        public GameManager GameManager
        {
            get { return mGameManager; }
        }

        private GraphicManager mGraphicManager;
        public GraphicManager GraphicManager
        {
            get { return mGraphicManager; }
        }

        public void Initialize(GameManager gameManager)
        {
            mGameManager = gameManager;
            mGraphicManager = GetComponent<GraphicManager>();
            mGraphicManager.Initialize();

            foreach (var uiPanel in Panels)
            {
                uiPanel.Initialize(this);
            }

            OpenPanel(PanelTypes.Hud);
        }

        public void OpenPanel(PanelTypes currentPanel)
        {
            Panels[(int)currentPanel].ShowPanel();
        }

        public void OpenPanel(PanelTypes currPanel, Color bgColor, string objName)
        {
            Panels[(int)currPanel].ShowPanel(bgColor, objName);
        }
    }
}
