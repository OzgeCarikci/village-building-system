﻿using UnityEngine;

namespace VillageBuilding
{
    public class UIPanel : VillageBuildingBehaviour
    {
        protected UIManager UIManager;

        public virtual void Initialize(UIManager uiManager)
        {
            UIManager = uiManager;
            CanvasGroup.Close();
        }

        public virtual void ShowPanel()
        {
            CanvasGroup.Open();
        }

        public virtual void ShowPanel(Color bgColor, string descText)
        {
            CanvasGroup.Open();
        }

        public virtual void HidePanel()
        {
            CanvasGroup.Close();
        }
    }
}
