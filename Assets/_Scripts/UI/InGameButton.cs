﻿using System;
using StructureDatas;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VillageBuilding
{
    public class InGameButton : Button
    {
        #region Fields

        protected UIManager mUIManager;

        private event Action mOnClick;

        private event Action<StructureItem> mOnUIStructureClick;

        private StructureItem mStructureItem;

        protected bool mHasInteraction;

        #endregion

        #region Methods

        public virtual void Initialize(UIManager UIManager, Action<StructureItem> onClickStructureItem, StructureItem StructureItem, bool active = true)
        {
            gameObject.SetActive(active);
            mUIManager = UIManager;
            mStructureItem = StructureItem;
            mOnUIStructureClick = onClickStructureItem;
        }

        public virtual void Initialize(Action onClick, bool active = true)
        {
            gameObject.SetActive(active);
            mOnClick = onClick;
        }

        public virtual void Initialize(UIManager UIManager, Action onClick, bool active = true)
        {
            gameObject.SetActive(active);
            mUIManager = UIManager;
            mOnClick = onClick;
        }

        public virtual void SetInteraction(bool interaction)
        {
            interactable = interaction;
            image.color = interaction ? Color.white : Color.gray;
            mHasInteraction = interaction;
        }

        #endregion

        #region Events

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!interactable)
                return;

            base.OnPointerClick(eventData);

            if (mOnClick != null)
                mOnClick();

            if (mOnUIStructureClick != null)
                mOnUIStructureClick(mStructureItem);
        }

        #endregion
    }
}
