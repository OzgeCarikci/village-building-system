﻿namespace VillageBuilding
{
    public class HudPanel : UIPanel
    {
        public InGameButton ButtonShop;
        public override void Initialize(UIManager uiManager)
        {
            base.Initialize(uiManager);
            ButtonShop.Initialize(UIManager, () => { UIManager.OpenPanel(PanelTypes.StructureShop); }, true);
        }

        public override void ShowPanel()
        {
            base.ShowPanel();
        }

        public override void HidePanel()
        {
            base.HidePanel();
        }
    }
}
