﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace VillageBuilding
{
    public class PhysicalDragThreshold : VillageBuildingBehaviour
    {
        private const float inchToCm = 2.54f;

        [SerializeField]
        private EventSystem eventSystem = null;

        [SerializeField]
        private float dragThresholdCM = 0.5f;

        public void Initialize(){
            if (eventSystem == null)
            {
                eventSystem = GetComponent<EventSystem>();
            }
            SetDragThreshold();
        }

        private void SetDragThreshold()
        {
            if (eventSystem != null)
            {
                eventSystem.pixelDragThreshold = (int)(dragThresholdCM * Screen.dpi / inchToCm);
            }
        }
    }
}
