﻿using UnityEngine;
using UnityEngine.UI;

namespace VillageBuilding
{
    public class StructurePopupPanel : UIPanel
    {
        public InGameButton ButtonClose;
        public Text DescText;
        public Image PopupBgImage;
        public override void Initialize(UIManager uiManager)
        {
            base.Initialize(uiManager);
            ButtonClose.Initialize(UIManager, OnClickCloseButton);
        }

        public void OnClickCloseButton()
        {
            HidePanel();
        }

        public override void ShowPanel()
        {
            base.ShowPanel();
        }

        public override void ShowPanel(Color bgColor, string descText)
        {
            DescText.text = descText;
            PopupBgImage.color = bgColor;

            base.ShowPanel(bgColor, descText);
        }

        public override void HidePanel()
        {
            base.HidePanel();
        }
    }
}
