﻿using StructureDatas;
using UnityEngine;
using UnityEngine.UI;

namespace VillageBuilding
{
    public class UIStructure : VillageBuildingBehaviour
    {
        public InGameButton Button;
        public Text NameText;
        public Image Image;
        public Image LockImage;

        private UIManager mUiManager;

        public StructureItem mStructureItem;
        
        public void Initialize(UIManager uiManager, StructureItem item, Sprite sprite)
        {
            mUiManager = uiManager;
            mStructureItem = item;

            Image.sprite = sprite;
            Button.image.color = mUiManager.GraphicManager.HexToColor(item.PopupColor);
            NameText.text = item.Name;
            RectTransform.localScale = Vector3.one;

            mUiManager.GameManager.LoadGeneratedUniqueId(item.Id);
        }
        
        public void Refresh(bool canBuild)
        {
            LockImage.enabled = !canBuild;
            Button.interactable = canBuild;
        }
    }
}
