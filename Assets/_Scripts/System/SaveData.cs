﻿using StructureDatas;

public class SaveData {

    public StructureItem StructureItem  { get; set; }
    public int[] Grids                  { get; set; }
}
