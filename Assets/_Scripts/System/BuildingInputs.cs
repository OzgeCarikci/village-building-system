﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace VillageBuilding
{
    public class BuildingInputs : VillageBuildingBehaviour
    {
        public Camera RayCamera;
        private RaycastHit mHit;
        private RaycastHit mPreviousHit;
        private Ray mRay;

        private GameManager mGameManager;
        public GameManager GameManager { get { return mGameManager; } }

        private Structure mSelectedStructure;
        private Grid mSelectedGrid;

        private LayerMask mGridLayer = 1 << 8;
        private LayerMask mStructureLayer = 1 << 9;

        public void Initialize(GameManager gameManager)
        {
            mGameManager = gameManager;
        }

        public void Update()
        {

            UpdateSelectionInput();
        }

        private void UpdateSelectionInput()
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;

            if (Input.GetMouseButtonDown(0))
            {
                if (IsRayHitTo(Keys.STRUCTURE, mStructureLayer))
                {
                    SelectStructure();
                }
            }
            else if (Input.GetMouseButton(0))
            {
                if (HasBuild())
                {
                    if (IsRayHitTo(Keys.GRID, mGridLayer))
                    {
                        if (IsGridChanged())
                        {
                            MoveStructure();
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (IsRayHitTo(Keys.STRUCTURE, mStructureLayer))
                {
                    PopupStructure();
                }

                mSelectedStructure = null;
                mSelectedGrid = null;
            }
        }

        private void SelectStructure()
        {
            mSelectedStructure = mHit.transform.parent.GetComponent<Structure>();
            mPreviousHit = mHit;
        }

        private bool HasBuild()
        {
            return (mSelectedStructure != null && !mSelectedStructure.HasBuild);
        }

        private void MoveStructure()
        {
            mSelectedGrid = mHit.transform.GetComponent<Grid>();
            mPreviousHit = mHit;

            var isGridsEmpty = GameManager.GridSystem.IsGridsEmpty(mSelectedGrid.Id, mSelectedStructure.GridSize);

            mSelectedStructure.SnapTo(mHit.transform.position, isGridsEmpty);
        }

        private void PopupStructure()
        {
            var hitStructure = mHit.transform.parent.GetComponent<Structure>();

            if (mSelectedStructure.StructureItem.Id == hitStructure.StructureItem.Id)
            {
                mSelectedStructure = hitStructure;
                mPreviousHit = mHit;

                if (mSelectedStructure.HasBuild)
                    mSelectedStructure.Select();
            }
        }

        private bool IsGridChanged()
        {
            return mHit.transform.gameObject != mPreviousHit.transform.gameObject;
        }

        private bool IsRayHitTo(string hitTag, int layer)
        {
            mRay = RayCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(mRay, out mHit, 100, layer))
            {
                if (mHit.collider.CompareTag(hitTag))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
