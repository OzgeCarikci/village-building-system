﻿using System.Collections.Generic;
using UnityEngine;
using StructureDatas;

namespace VillageBuilding
{
    public class GameManager : VillageBuildingBehaviour
    {
        public GridSystem GridSystem;
        public UIManager UiManager;
        public BuildingInputs BuildingInputs;
        public PhysicalDragThreshold PhysicalDragThreshold;

        public GameObject CommonPrefab;
        public GameObject UniquePrefab;

        public List<int> GeneratedUniqueIds = new List<int>();

        private GameObject mGeneratedStructure;

        private Structure mCurrentStructure;

        public Structure CurrentStructure
        {
            get { return mCurrentStructure; }
            set { mCurrentStructure = value; }
        }

        public void Awake()
        {
            Application.targetFrameRate = 30;

            UiManager.Initialize(this);
            GridSystem.Initialize(this);
            BuildingInputs.Initialize(this);
            PhysicalDragThreshold.Initialize();
        }

        public void InstantiateStructure(StructureItem structureItem, bool setPosition = true)
        {
            if (!IsCached(structureItem))
            {
                mGeneratedStructure = GetStructure((StructureTypes)structureItem.Type);
            }

            CurrentStructure = mGeneratedStructure.GetComponent<Structure>();
            CurrentStructure.Init(this, structureItem);

            if (setPosition)
            {
                var gridId = GridSystem.GetAvailableGrid(CurrentStructure.GridSize);
                var pos = GridSystem.GetGridPosition(gridId);
                CurrentStructure.SnapTo(pos, true);
            }
        }

        public void InstantiateStructure(StructureItem structureItem, SaveData saveData)
        {
            InstantiateStructure(structureItem, false);

            var gridMinId = saveData.Grids.GetMinValue();
            var position = GridSystem.GetGridPosition(gridMinId);
            CurrentStructure.SnapTo(position, true);
            CurrentStructure.BuildLoadGame(saveData);
            CurrentStructure = null;
        }

        private GameObject GetStructure(StructureTypes type)
        {
            switch (type)
            {
                case StructureTypes.Common:
                    return Instantiate(CommonPrefab);

                case StructureTypes.Unique:
                    return Instantiate(UniquePrefab);
            }
            return null;
        }

        private bool IsCached(StructureItem item)
        {
            if (CurrentStructure == null) return false;

            if (CurrentStructure.StructureItem.Type == item.Type)
            {
                mGeneratedStructure = CurrentStructure.gameObject;
                return true;
            }
            // We can cache generated object instead of destroying.
            Destroy(CurrentStructure.gameObject);
            return false;
        }

        public void LoadGeneratedUniqueId(int id)
        {
            var isGenerated = PlayerPrefs.GetInt(id.ToString(), 0) == 1;

            if (isGenerated)
                GeneratedUniqueIds.Add(id);
        }
    }
}
