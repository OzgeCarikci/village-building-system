﻿using UnityEngine;

public static class Extensions
{
    public static void Open(this CanvasGroup canvas)
    {
        canvas.alpha = 1;
        canvas.interactable = true;
        canvas.blocksRaycasts = true;
    }

    public static void Close(this CanvasGroup canvas)
    {
        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;
    }

    public static int GetMinValue(this int[] array)
    {
        int minValue = array[0];
        foreach (var value in array)
        {
            if (minValue > value)
            {
                minValue = value;
            }
        }

        return minValue;
    }
}
