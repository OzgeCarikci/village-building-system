﻿using UnityEngine;
using UnityEngine.UI;

public class VillageBuildingBehaviour : MonoBehaviour
{
    private Transform mTransform;
    public Transform Transform
    {
        get
        {
            if (mTransform == null)
            {
                mTransform = base.transform;
            }

            return mTransform;
        }
    }

    private Animator mAnimator;
    public Animator Animator
    {
        get
        {
            if (mAnimator == null)
            {
                mAnimator = base.GetComponent<Animator>();
            }

            return mAnimator;
        }
    }

    private RectTransform mRectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (mRectTransform == null)
            {
                mRectTransform = base.GetComponent<RectTransform>();
            }

            return mRectTransform;
        }
    }

    private Rigidbody2D rigidBody;
    public Rigidbody2D RigidBody
    {
        get
        {
            if (rigidBody == null)
            {
                rigidBody = base.GetComponent<Rigidbody2D>();
            }

            return rigidBody;
        }
    }

    private CanvasGroup mCanvasGroup;
    public CanvasGroup CanvasGroup
    {
        get
        {
            if (mCanvasGroup == null)
            {
                mCanvasGroup = base.GetComponent<CanvasGroup>();
            }

            return mCanvasGroup;
        }
    }

    private Text mText;
    public Text Text
    {
        get
        {
            if (mText == null)
            {
                mText = base.GetComponent<Text>();
            }

            return mText;
        }
    }

    private Text mChildText;
    public Text ChildText
    {
        get
        {
            if (mChildText == null)
            {
                mChildText = base.GetComponentInChildren<Text>();
            }

            return mChildText;
        }
    }

    private Camera mCamera;

    public Camera Camera
    {
        get
        {
            if (mCamera == null)
            {
                mCamera = base.GetComponent<Camera>();
            }

            return mCamera;
        }
    }
}
