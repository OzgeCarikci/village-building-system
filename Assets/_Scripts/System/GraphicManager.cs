﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace VillageBuilding
{
    public class GraphicManager : VillageBuildingBehaviour
    {
        public List<Sprite> StructureSprites = new List<Sprite>();
        public List<string> StructureImageNames = new List<string>();
        public List<Material> StructureMaterials = new List<Material>();

        // imageName, sprite
        public Dictionary<string, Sprite> StructuresSpritesDict = new Dictionary<string, Sprite>();
        private Dictionary<string, Material> StructuresMatDict = new Dictionary<string, Material>();
        public void Initialize()
        {
            for (int index = 0; index < StructureSprites.Count; index++)
            {
                StructuresSpritesDict.Add(StructureImageNames[index], StructureSprites[index]);
                StructuresMatDict.Add(StructureImageNames[index], StructureMaterials[index]);
            }
        }

        public Sprite GetStructureSprite(string spriteName)
        {
            return StructuresSpritesDict[spriteName];
        }

        public Material GetStructureMaterial(string matName)
        {
            return StructuresMatDict[matName];
        }

        public Color HexToColor(string hex)
        {
            byte r = byte.Parse(hex.Substring(0, 2), NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }
    }
}
