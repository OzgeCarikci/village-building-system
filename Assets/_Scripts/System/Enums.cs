﻿public enum PanelTypes : byte
{
    Hud = 0,
    StructureShop,
    StructurePopup
}

public enum StructureTypes : byte
{
    Common = 0,
    Unique
}