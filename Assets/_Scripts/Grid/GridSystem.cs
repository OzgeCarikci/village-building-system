﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using StructureDatas;

namespace VillageBuilding
{
    public class GridSystem : VillageBuildingBehaviour
    {
        public GameObject GridPrefab;
        public int TotalRowCount;
        public int TotalColumnCount;
        public Vector2 GridUnitInterval;

        private List<SaveData> SavedData = new List<SaveData>();
        private Dictionary<int, Grid> mGrids = new Dictionary<int, Grid>();
        private Dictionary<int, Grid> mSelectedGrids = new Dictionary<int, Grid>();

        private float depthInterval = 0.005f;
        private GameManager mGameManger;

        public void Initialize(GameManager gameManager)
        {
            mGameManger = gameManager;
            GenerateGrids();
            LoadSavedGame();
        }

        private void LoadSavedGame()
        {
            var data = PlayerPrefs.GetString(Keys.SAVE_STRUCTURE, null);

            if (string.IsNullOrEmpty(data)) return;

            SavedData.Clear();
            SavedData = JsonConvert.DeserializeObject<List<SaveData>>(data);
            BuildStructure();
        }

        private void BuildStructure()
        {
            foreach (var item in SavedData)
            {
                mGameManger.InstantiateStructure(item.StructureItem, item);
            }
        }

        public Vector3 GetGridPosition(int id)
        {
            return mGrids[id].transform.position;
        }

        private void SaveGameAfterBuilding(SaveData item)
        {
            SavedData.Add(item);
            var json = JsonConvert.SerializeObject(SavedData);

            PlayerPrefs.SetString(Keys.SAVE_STRUCTURE, json);
            PlayerPrefs.Save();
        }

        private void GenerateGrids()
        {
            var multiplier = 0;
            var zPos = 0f;

            for (var row = 0; row < TotalRowCount; row++)
            {
                for (var col = 0; col < TotalColumnCount; col++)
                {
                    var grid = Instantiate(GridPrefab);
                    var id = (row * TotalRowCount) + col;
                    var pos = new Vector3((col - multiplier) * GridUnitInterval.x, zPos, (col + multiplier) * GridUnitInterval.y);
                    var gridProp = grid.GetComponent<Grid>();

                    grid.transform.position = pos;
                    grid.transform.parent = transform;
                    grid.name = id.ToString();

                    mGrids.Add(id, gridProp);
                    gridProp.Initialize(id);

                }
                multiplier++;
                zPos -= depthInterval;
            }
        }

        public bool IsGridsEmpty(int gridId, Vector2 structureSize)
        {
            mSelectedGrids.Clear();
            for (var row = 0; row < structureSize.y; row++)
            {
                for (var col = 0; col < structureSize.x; col++)
                {
                    var tempGridId = gridId + col + (row * TotalRowCount);

                    if (!mGrids.ContainsKey(tempGridId) || !mGrids[tempGridId].IsEmpty || IsLastGrid(gridId, (int)structureSize.x))
                    {
                        return false;
                    }

                    mSelectedGrids.Add(mGrids[tempGridId].Id, mGrids[tempGridId]);
                }
            }
            return true;
        }

        private bool IsLastGrid(int gridId, int gridColumnCount)
        {
            if (gridColumnCount > 1)
            {
                return (mGrids[gridId].Id + 1) % TotalColumnCount == 0;
            }

            return false;
        }

        public void FillGrids(StructureItem item)
        {
            var grids = new int[mSelectedGrids.Count];
            int index = 0;

            foreach (var grid in mSelectedGrids)
            {
                grid.Value.IsEmpty = false;
                grids[index++] = grid.Value.Id;

                mGrids.Remove(grid.Key);
            }

            var data = new SaveData() { Grids = grids, StructureItem = item };
            SaveGameAfterBuilding(data);

        }

        public void LoadFullGrids(SaveData saveData)
        {
            for (int i = 0; i < saveData.Grids.Length; i++)
            {
                mGrids[saveData.Grids[i]].IsEmpty = false;
                mGrids.Remove(saveData.Grids[i]);
            }
        }

        public int GetAvailableGrid(Vector2 structureSize)
        {
            foreach (var grid in mGrids)
            {
                if (IsGridsEmpty(grid.Value.Id, structureSize))
                {
                    return grid.Value.Id;
                }
            }
            // We can ignore generating structure.
            Debug.LogWarning("There is no available space, don't generate structure");
            return 0;
        }
    }
}

