﻿namespace VillageBuilding
{
    public class Grid : VillageBuildingBehaviour
    {
        public int Id;
        public bool IsEmpty = true;

        private GridSystem mGridSystem;

        public void Initialize(int id)
        {
            Id = id;
        }
    }
}
