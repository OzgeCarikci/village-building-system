﻿using UnityEngine;
using StructureDatas;

namespace VillageBuilding
{
    public class Structure : VillageBuildingBehaviour
    {
        public StructureItem StructureItem;

        public Color PopupColor;
        public Vector2 GridSize;

        public InGameButton ButtonBuildApply;
        public InGameButton ButtonBuildCancel;

        public CanvasGroup ButtonBuildConfirm;

        public MeshRenderer MeshRenderer;

        public bool HasBuild = false;

        protected GameManager mGameManager;

        protected Vector3 mPositionDecreaseRate = new Vector3(0f, 0.1f, 0f);

        public virtual void Init(GameManager gameManager, StructureItem item)
        {
            mGameManager = gameManager;

            StructureItem = item;
            PopupColor = mGameManager.UiManager.GraphicManager.HexToColor(item.PopupColor);
            MeshRenderer.material = mGameManager.UiManager.GraphicManager.GetStructureMaterial(StructureItem.ImageName);

            ButtonBuildCancel.Initialize(Cancel);
            ButtonBuildApply.Initialize(Build);
        }

        public virtual void Build()
        {
            HasBuild = true;

            if (mGameManager.CurrentStructure != null)
            {
                mGameManager.CurrentStructure = null;
            }

            ButtonBuildConfirm.Close();
            mGameManager.GridSystem.FillGrids(StructureItem);
            SetLastPosition();
        }

        public virtual void BuildLoadGame(SaveData saveData)
        {
            HasBuild = true;
            ButtonBuildConfirm.Close();
            mGameManager.GridSystem.LoadFullGrids(saveData);
            SetLastPosition();
        }

        public virtual void Cancel()
        {
            Destroy(gameObject);
        }

        public virtual void Select()
        {
            mGameManager.UiManager.OpenPanel(PanelTypes.StructurePopup, PopupColor, StructureItem.Name);
        }

        public virtual void SnapTo(Vector3 pos, bool canBuild)
        {
            ButtonBuildApply.interactable = canBuild;
        }

        public virtual void SetLastPosition()
        {
            transform.position -= mPositionDecreaseRate;
        }
    }
}
