﻿using System.Collections.Generic;

namespace StructureDatas
{
    public class StructureData
    {
        public List<StructureItem> Structures { get; set; }
    }
}
