﻿using UnityEngine;

namespace VillageBuilding
{
    public class Unique : Structure
    {
        public override void SnapTo(Vector3 pos, bool canBuild)
        {
            base.SnapTo(pos, canBuild);
            // After we set the graphics, we can use a standard space.
            transform.position = new Vector3(pos.x, pos.y + .85f, pos.z + .5f);
        }

        public override void Build()
        {
            base.Build();
            PlayerPrefs.SetInt(StructureItem.Id.ToString(), 1);
            PlayerPrefs.Save();
            mGameManager.GeneratedUniqueIds.Add(StructureItem.Id);
        }
    }
}
