﻿using UnityEngine;

namespace VillageBuilding
{
    public class Common : Structure
    {
        public override void SnapTo(Vector3 pos, bool canBuild)
        {
            base.SnapTo(pos, canBuild);
            // After we set the graphics, we can use a standard space.
            transform.position = new Vector3(pos.x, pos.y + .85f, pos.z);
        }
    }
}
